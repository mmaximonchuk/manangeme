import React, { useState } from "react";
import { Link, graphql } from "gatsby";
import Image from "../components/Image";
import { Swiper, SwiperSlide } from "swiper/react";

import Layout from "../components/Layout";
import IconBitbucket from "../assets/images/customers/bitbucket.inline.svg";
import IconWatch from "../assets/images/customers/watch.inline.svg";
import IconFacebook from "../assets/images/customers/facebook.inline.svg";
import IconAtlassian from "../assets/images/customers/atlassian.inline.svg";
import IconAudi from "../assets/images/customers/audi.inline.svg";
import IconPlayManage from "../assets/images/plan-and-manage/icon-play.inline.svg";
// import imgAsideManage from "../assets/images/plan-and-manage/illustration.png";
import imgOverview from "../assets/images/features/overview.svg";
import imgFiles from "../assets/images/features/files.svg";
import imgMeetingChats from "../assets/images/features/meeting-chats.svg";
import imgSaveEvents from "../assets/images/features/save-events.svg";
import imgIncreaseStats from "../assets/images/stats/increase.svg";
import imgUsersStats from "../assets/images/stats/users.svg";
import imgBlogBig from "../assets/images/blog/bigone.png";
import imgBlogSm1 from "../assets/images/blog/sm1.png";
import imgBlogSm2 from "../assets/images/blog/sm2.png";
import ImgArrowDown from "../assets/images/faq/arr-down.inline.svg";
import imgPersonAvatar from "../assets/images/customers-quotes/person.jpg";
// styles
import "swiper/swiper.scss";
// markup
const accordeonData = [
  {
    id: 1,
    title: "Lorem repudiandae ne nec?",
    description: `Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                    diceret persecuti.`,
  },
  {
    id: 2,
    title: "Lorem repudiandae ne nec?",
    description: `Quidam vocibus eum ne, erat consectetuer voluptatibus ut
    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
    diceret persecuti.`,
  },
  {
    id: 3,
    title: "Lorem repudiandae ne nec?",
    description: `Quidam vocibus eum ne, erat consectetuer voluptatibus ut
    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
    diceret persecuti.`,
  },
  {
    id: 4,
    title: "Lorem repudiandae ne nec?",
    description: `Quidam vocibus eum ne, erat consectetuer voluptatibus ut
    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
    diceret persecuti.`,
  },
];

const IndexPage = ({ data }) => {
  const [openedAccordeons, setOpenedAccordeons] = useState([]);

  const handleAccordeonToOpen = (itemId) => {
    if (!openedAccordeons.includes(itemId)) {
      const { id } = accordeonData.filter((item) => itemId === item.id)[0];
      setOpenedAccordeons([...openedAccordeons, id]);
    } else {
      const newItemToReset = openedAccordeons.filter((id) => itemId !== id);
      setOpenedAccordeons([...newItemToReset]);
    }
  };
  // const params = {
  //   pagination: {
  //     el: '.swiper-pagination',
  //     clickable: true,
  //     renderBullet: (index, className) => {
  //       return '<span class="' + className + '">' + (index + 1) + '</span>';
  //     }
  //   }
  // }
  return (
    <Layout
      description={
        "Increase your productivity, join our society and enhance your mananging skills"
      }
    >
      <main className="home-page">
        {/* ----------- hero-image ----------- */}
        <section className="section-outer section-hero-image">
          <Swiper
            className="slider-hero-image"
            onSlideChange={() => console.log("slide change")}
            onSwiper={(swiper) => console.log(swiper)}
            pagination={{ clickable: true }}
          >
            <SwiperSlide className="slider-hero-image__item">
              <div className="slider-hero-image__item">
                <div className="section-inner slider-hero-image__item--content text-center vertical-indent vertical-indent--l">
                  <h5>PLAN YOUR LIFE</h5>
                  <h2>
                    Increase your <span>productivity</span>
                  </h2>
                  <div className="text">
                    Brute laoreet efficiendi id his, ea illum nonumes luptatum
                    pro. Usu atqui laudem an, insolens gubergren similique est
                    cu. Et vel modus congue vituperata.
                  </div>
                  <div className="btn-icon-play-wrapper">
                    <a href="#">
                      <i className="icon-play">
                        <IconPlayManage />
                      </i>
                    </a>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide className="slider-hero-image__item">
              <div className="slider-hero-image__item">
                <div className="section-inner slider-hero-image__item--content text-center vertical-indent vertical-indent--l">
                  <h5>PLAN YOUR LIFE</h5>
                  <h2>
                    Increase your <span>productivity</span>
                  </h2>
                  <div className="text">
                    Brute laoreet efficiendi id his, ea illum nonumes luptatum
                    pro. Usu atqui laudem an, insolens gubergren similique est
                    cu. Et vel modus congue vituperata.
                  </div>
                  <div className="btn-icon-play-wrapper">
                    <a href="#">
                      <i className="icon-play">
                        <IconPlayManage />
                      </i>
                    </a>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide className="slider-hero-image__item">
              <div className="slider-hero-image__item">
                <div className="section-inner slider-hero-image__item--content text-center vertical-indent vertical-indent--l">
                  <h5>PLAN YOUR LIFE</h5>
                  <h2>
                    Increase your <span>productivity</span>
                  </h2>
                  <div className="text">
                    Brute laoreet efficiendi id his, ea illum nonumes luptatum
                    pro. Usu atqui laudem an, insolens gubergren similique est
                    cu. Et vel modus congue vituperata.
                  </div>
                  <div className="btn-icon-play-wrapper">
                    <a href="#">
                      <i className="icon-play">
                        <IconPlayManage />
                      </i>
                    </a>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <SwiperSlide className="slider-hero-image__item">
              <div className="slider-hero-image__item">
                <div className="section-inner slider-hero-image__item--content text-center vertical-indent vertical-indent--l">
                  <h5>PLAN YOUR LIFE</h5>
                  <h2>
                    Increase your <span>productivity</span>
                  </h2>
                  <div className="text">
                    Brute laoreet efficiendi id his, ea illum nonumes luptatum
                    pro. Usu atqui laudem an, insolens gubergren similique est
                    cu. Et vel modus congue vituperata.
                  </div>
                  <div className="btn-icon-play-wrapper">
                    <a href="#">
                      <i className="icon-play">
                        <IconPlayManage />
                      </i>
                    </a>
                  </div>
                </div>
              </div>
            </SwiperSlide>
            <div className="dots swiper-pagination">
              {/* <li className="dots__item dots__item--active"></li>
              <li className="dots__item"></li>
              <li className="dots__item"></li> */}
            </div>
          </Swiper>
        </section>
        <section></section>
        {/* ----------- customers ----------- */}
        <section className="section-outer section-customers">
          <div className="section-inner vertical-indent vertical-indent--m">
            <a href="#" target="_blank">
              <IconBitbucket />
            </a>
            <a href="#" target="_blank">
              <IconWatch />
            </a>
            <a href="#" target="_blank">
              <IconFacebook />
            </a>
            <a href="#" target="_blank">
              <IconAtlassian />
            </a>
            <a href="#" target="_blank">
              <IconAudi />
            </a>
          </div>
        </section>

        {/* ----------- plan-and-man0age ----------- */}
        <section className="section-outer section-plan-and-manage">
          <div className="section-inner vertical-indent vertical-indent--l">
            <div className="section-plan-and-manage__content">
              <div className="aside-image">
                <Image imageUrl={"plan-and-manage/illustration.png"} />
              </div>
              <h5>DESKTOP AND MOBILE APP</h5>
              <h3>
                <span>Plan</span> and <span>manage</span>
              </h3>
              <div className="text">
                Brute laoreet efficiendi id his, ea illum nonumes luptatum pro.
                Usu atqui laudem an, insolens gubergren similique est cu. Et vel
                modus congue vituperata. Solum patrioque no sea. Mea ex malis
                mollis oporteat. Eum an expetenda consequat.
              </div>
              <div className="btn-group">
                <a href="#" className="btn btn--secondary">
                  View video <IconPlayManage />
                </a>
                <a href="#" className="btn btn--no-bg">
                  See features
                </a>
              </div>
            </div>
          </div>
        </section>

        {/* ----------- features ----------- */}
        <section className="section-outer section-features">
          <div className="section-inner text-center vertical-indent vertical-indent--l">
            <h5>ABOUT US</h5>
            <h4>Read about our app</h4>

            <ul className="features-list">
              <li className="features-list__item">
                <div className="icon">
                  <img src={imgOverview} alt="imgOverview" />
                </div>
                <p className="title">Overview</p>
                <p className="text">
                  Brute laoreet efficiendi id his, ea illum nonumes luptatum
                  pro.
                </p>
              </li>
              <li className="features-list__item">
                <div className="icon">
                  <img src={imgFiles} alt="imgFiles" />
                </div>
                <p className="title">Files</p>
                <p className="text">
                  No vim nulla vitae intellegat. Ei enim error ius, solet
                  atomorum conceptam ex has.
                </p>
              </li>
              <li className="features-list__item">
                <div className="icon">
                  <img src={imgMeetingChats} alt="imgMeetingChats" />
                </div>
                <p className="title">Meeting chats</p>
                <p className="text">
                  Vim ne tacimates neglegentur. Erat diceret omittam at est.
                </p>
              </li>
              <li className="features-list__item">
                <div className="icon">
                  <img src={imgSaveEvents} alt="imgSaveEvents" />
                </div>
                <p className="title">Save events</p>
                <p className="text">Nisl idque mel ea, nominati voluptatum.</p>
              </li>
            </ul>

            <div className="btns">
              <a href="#" className="btn btn--danger">
                Read more
              </a>
              <span className="or-block">OR</span>
              <a href="#" className="btn btn--primary">
                Get started
              </a>
            </div>
          </div>
        </section>

        {/* ----------- stats ----------- */}
        <section className="section-outer section-stats">
          <div className="section-stats__left">
            <div className="icon">
              <img src={imgIncreaseStats} alt="Increase Your Productivity" />
            </div>
            <h2 className="number">89%</h2>
            <div className="text">
              Customers who have increased their productivity
            </div>
          </div>
          <div className="section-stats__right">
            <div className="icon">
              <img src={imgUsersStats} alt="Increase Your Productivity" />
            </div>
            <h2 className="number">3123</h2>
            <div className="text">Users who have used our application</div>
          </div>
        </section>

        {/* ----------- blog ----------- */}
        <section className="section-outer section-blog">
          <div className="section-inner vertical-indent vertical-indent--l">
            <h5>OUR RESOURCES</h5>
            <h4>Start reading our blog</h4>
            <div className="slider-wrapper">
              <ul className="slider-blog">
                <li className="slider-blog__item">
                  <div className="gallery">
                    <Link to="/" className="image-link image-link--big">
                      <img src={imgBlogBig} alt="Blog image 1" />
                    </Link>
                    <Link to="/" className="image-link image-link--sm-1">
                      <img src={imgBlogSm1} alt="Blog image 2" />
                    </Link>
                    <Link to="/" className="image-link image-link--sm-2">
                      <img src={imgBlogSm2} alt="Blog image 3" />
                    </Link>
                  </div>
                  <div className="content">
                    <h3 className="title">How to start planning</h3>
                    <p className="text">
                      Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                      nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                      diceret persecuti. Natum petentium principes mei ea. Tota
                      everti periculis vis ei, quas tibique pro at, eos ut
                      decore ...
                    </p>
                    <div className="btn-group">
                      <a href="#" className="btn btn--primary">
                        Read now
                      </a>
                      <a href="#" className="btn btn--no-bg">
                        Add to your bookmarks
                      </a>
                    </div>
                  </div>
                </li>
              </ul>
              <a href="#" className="btn-blog btn-blog--prev"></a>
              <a href="#" className="btn-blog btn-blog--next"></a>
            </div>
            <ul className="dots">
              <li className="dots__item dots__item--active"></li>
              <li className="dots__item"></li>
              <li className="dots__item"></li>
            </ul>
          </div>
        </section>

        {/* ----------- quotes ----------- */}
        <section className="section-outer section-quotes">
          <div className="section-inner vertical-indent vertical-indent--l">
            <div className="section-quotes__left">
              <h5>TESTIMONIALS</h5>
              <h3>Customers's quotes</h3>
              <p className="text">
                Brute laoreet efficiendi id his, ea illum nonumes luptatum pro.
                Usu atqui laudem an.
              </p>
            </div>
            <div className="section-quotes__right">
              <ul className="slider-quotes">
                <li className="slider-quotes__item">
                  <div className="item__text">
                    Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                    diceret persecuti.
                  </div>
                  <div className="item__photo">
                    <img src={imgPersonAvatar} alt="avatar" />
                  </div>
                </li>
              </ul>
              <ul className="dots">
                <li className="dots__item dots__item--active"></li>
                <li className="dots__item"></li>
                <li className="dots__item"></li>
              </ul>
            </div>
          </div>
        </section>

        {/* ----------- faq ----------- */}

        <section className="section-outer section-faq">
          <div className="section-inner vertical-indent vertical-indent--l">
            <h5 className="text-center">Customer help</h5>
            <h4 className="text-center">Frequently asked questions</h4>
            <ul className="faq-accordion">
              {accordeonData.map((item) => {
                return (
                  <li
                    key={item.id}
                    onClick={() => handleAccordeonToOpen(item.id)}
                    className={`faq-accordion__item ${
                      openedAccordeons.includes(item.id)
                        ? `faq-accordion__item--active`
                        : ""
                    }`}
                  >
                    <div className="faq-accordion__item-trigger">
                      <ImgArrowDown className="icon" />
                      <div className="text">Lorem repudiandae ne nec?</div>
                    </div>
                    <div className="faq-accordion__item-content ">
                      <div className="text">
                        Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                        nam. Eu usu vidit tractatos, vero tractatos ius an, in
                        mel diceret persecuti.
                      </div>
                      <div className="button">
                        <button className="btn btn--grayscale">
                          Go to documentation
                        </button>
                      </div>
                    </div>
                  </li>
                );
              })}

              {/* <li className="faq-accordion__item faq-accordion__item--active">
                <div className="faq-accordion__item-trigger">
                  <ImgArrowDown className="icon" />
                  <div className="text">Lorem repudiandae ne nec?</div>
                </div>
                <div className="faq-accordion__item-content">
                  <div className="text">
                    Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                    diceret persecuti.
                  </div>
                  <div className="button">
                    <button className="btn btn--grayscale">
                      Go to documentation
                    </button>
                  </div>
                </div>
              </li>
              <li className="faq-accordion__item">
                <div className="faq-accordion__item-trigger">
                  <ImgArrowDown className="icon" />
                  <div className="text">Lorem repudiandae ne nec?</div>
                </div>
                <div className="faq-accordion__item-content">
                  <div className="text">
                    Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                    diceret persecuti.
                  </div>
                  <div className="button">
                    <button className="btn btn--grayscale">
                      Go to documentation
                    </button>
                  </div>
                </div>
              </li>
              <li className="faq-accordion__item">
                <div className="faq-accordion__item-trigger">
                  <ImgArrowDown className="icon" />
                  <div className="text">Lorem repudiandae ne nec?</div>
                </div>
                <div className="faq-accordion__item-content">
                  <div className="text">
                    Quidam vocibus eum ne, erat consectetuer voluptatibus ut
                    nam. Eu usu vidit tractatos, vero tractatos ius an, in mel
                    diceret persecuti.
                  </div>
                  <div className="button">
                    <button className="btn btn--grayscale">
                      Go to documentation
                    </button>
                  </div>
                </div>
              </li> */}
            </ul>
          </div>
        </section>
        {/* ----------- get-started ----------- */}

        <section className="section-outer section-get-started">
          <div className="section-inner text-center vertical-indent vertical-indent--l">
            <h5>PLAN TOUR LIFE</h5>
            <h2>
              Get <span>started</span> now
            </h2>
            <div className="text">
              Brute laoreet efficiendi id his, ea illum nonumes luptatum pro.
              Usu atqui laudem an, insolens gubergren similique est cu. Et vel
              modus congue vituperata.
            </div>
            <div className="btn-group">
              <a
                href="#"
                className="btn btn--secondary btn--secondary-inverted"
              >
                View pricing
              </a>
              <a href="#" className="btn btn--no-bg btn--no-bg-inverted">
                Read documentation
              </a>
            </div>
          </div>
        </section>
        {/* ----------- news-letter ----------- */}

        <section className="section-outer section-news-letter">
          <div className="section-inner vertical-indent vertical-indent--l">
            <div className="section-news-letter__left">
              <p className="title">Sign up for newsletter</p>
              <p className="text">
                Cu qui soleat partiendo urbanitas. Eum aperiri indoctum eu,
                homero alterum.
              </p>
            </div>
            <div className="section-news-letter__right">
              <form className="form-newsletter">
                <label>
                  <input
                    className="input input--size-lg"
                    type="text"
                    placeholder="Email address"
                  />
                  <button
                    type="submit"
                    className="btn btn--size-lg btn--secondary"
                  >
                    Save me
                  </button>
                </label>
              </form>
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
};

// export const query = graphql`
//   query {
//     file(relativePath: { eq: "plan-and-manage/illustration.png" }) {
//       childImageSharp {
//         fluid(maxWidth: 845, quality: 100) {
//           ...GatsbyImageSharpFluid
//         }
//       }
//     }
//   }
// `

export default IndexPage;
