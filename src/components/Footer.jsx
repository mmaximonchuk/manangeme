import React, { useState } from "react";
import { Link } from "gatsby";
import ImgFacebook from "../assets/images/facebook.inline.svg";
import ImgTelegram from "../assets/images/telegram.inline.svg";
import ImgTwitter from "../assets/images/twitter.inline.svg";
import ImgMedium from "../assets/images/medium.inline.svg";

const Footer = () => {
  return (
    <footer className="section-outer section-footer">
      <div className="section-inner section-inner--wide vertical-indent vertical-indent--s">
        <div className="main-navigation">
          <Link to="/" className="logo">
            <span className="visually-hidden">Logo</span>
          </Link>
          {/* <button className="btn btn--no-bg btn-burger">
            <span className="btn-burger__line"/>
            <span className="btn-burger__line"/>
            <span className="btn-burger__line"/>
          </button> */}
          <HeaderInnerContent />
        </div>
        <div className="copyright">Copyright © 2021 by Random site</div>
      </div>
    </footer>
  );
};

function HeaderInnerContent(props) {
  return (
    <div className="main-navigation__inner-wrap">
      <nav className="nav">
        <ul className="nav-wrapper">
          <li className="nav-wrapper__item">
            <Link
              to="/"
              className="nav-wrapper__item-link nav-wrapper__item-link--active"
            >
              Home
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Blog
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Features
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Pricing
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Documentation
            </Link>
          </li>
        </ul>
      </nav>
      <div className="socials">
        <ul className="socials-wrapper">
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgFacebook />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgTelegram />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgTwitter />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgMedium />
            </a>
          </li>
        </ul>
      </div>
      <a href="/" className="btn btn--primary">
        Get started
      </a>
    </div>
  );
}

export default Footer;
