import React from 'react';
import Header from './Header'
import Footer from './Footer'
import iconShortcut from '../assets/images/logo.svg'
import SEO from './SEO';

import '../assets/styles/main.scss';

const Layout = ({ children, title, description, location }) => {
    return (
      <>
        <SEO title={'Homepage'} description={description} url={''} iconShortcut={iconShortcut} />
  
        <Header />
        <div>{children}</div>
        <Footer />
      </>
    );
  };
  

export default Layout;