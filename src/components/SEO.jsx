import React from 'react';
import { Helmet } from 'react-helmet';

const SEO = ({ title, description, url, iconShortcut }) => {
  const seoTitle = title || '';
  const seoDescription = description || '';
  const seoURL = url || '';
  const schemaOrgJSONLD = [
    {
      '@context': 'http://schema.org',
      '@type': 'Website',
    //   url: config.siteUrl,
      name: seoTitle,
    //   alternateName: config.siteTitleShort,
    },
  ];

  return (
    <Helmet>
      <title>{seoTitle}</title>
      <meta name="title" content={seoTitle} />
      <meta name="description" content={seoDescription} />
      <link rel="shortcut icon" href={iconShortcut} type="image/svg" />

      {/* Schema.org tags */}
      <script type="application/ld+json">{JSON.stringify(schemaOrgJSONLD)}</script>
      {/* OpenGraph tags */}
      <meta property="og:title" content={seoTitle} />
      <meta property="og:description" content={seoDescription} />
      <meta property="og:type" content={'website'} />
      <meta property="og:url" content={seoURL} />
      <meta property="og:locale" content="en" />
      {/* <meta property="og:site_name" content={config.siteTitleShort} /> */}
      {/* <meta property="og:image" content={seoImage.replace('https', 'http')} /> */}
      {/* <meta property="og:image:secure_url" content={seoImage} /> */}
      <meta property="og:image:width" content="512" />
      <meta property="og:image:height" content="512" />
      {/* <meta name="og:email" content={config.userEmail} /> */}
      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:title" content={seoTitle} />
      <meta name="twitter:description" content={seoDescription} />
      {/* <meta name="twitter:site" content={config.userTwitter || config.siteUrl} /> */}
      {/* <meta name="twitter:image" content={seoImage} /> */}
      {/* <meta name="twitter:creator" content={config.userTwitter || config.siteUrl} /> */}
      <meta name="twitter:domain" content="www.tefl-iberia.com" />
      <meta name="twitter:url" content={seoURL} />
      {/* <link rel="dns-prefetch" href="https://fonts.gstatic.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous" /> */}

      {/* <!-- Global site tag (gtag.js) - Google Analytics --> */}
      {/* <script async src="https://www.googletagmanager.com/gtag/js?id=UA-25594495-1"></script> */}
      <script>
        {`window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-25594495-1');`}
      </script>

      {/* <!-- Hotjar Tracking Code for www.tefl-iberia.com --> */}
      <script>
        {`(function(h,o,t,j,a,r){
              h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
              h._hjSettings={hjid:2115294,hjsv:6};
              a=o.getElementsByTagName('head')[0];
              r=o.createElement('script');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
          })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');`}
      </script>
    </Helmet>
  );
};

export default SEO;
