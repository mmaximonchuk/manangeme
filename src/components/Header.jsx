import React, { useState, useEffect } from "react";
import { Link } from "gatsby";
import ImgFacebook from "../assets/images/facebook.inline.svg";
import ImgTelegram from "../assets/images/telegram.inline.svg";
import ImgTwitter from "../assets/images/twitter.inline.svg";
import ImgMedium from "../assets/images/medium.inline.svg";

const Header = () => {
  const [isMobileOpen, setIsMobileOpen] = useState(false);

  const hideScroll = () => {
    const body = document.querySelector("body");
    body.paddingRight = `${getScrollbarWidth()}px`;
    body.style.overflow = "hidden";
  };

  const showScroll = () => {
    const body = document.querySelector("body");
    body.paddingRight = "";
    body.style.overflow = "visible";
  };

  const getScrollbarWidth = () => {
    const outer = document.createElement("div");
    outer.style.position = "absolute";
    outer.style.top = "-9999px";
    outer.style.width = "50px";
    outer.style.height = "50px";
    outer.style.overflow = "scroll";
    outer.style.visibility = "hiddern";
    document.querySelector("body").append(outer);
    const scrollBarWidth = outer.offsetWidth - outer.clientWidth;
    outer.remove();

    return scrollBarWidth;
  };

  const resetNav = () => {
    setIsMobileOpen(false);
  };

  useEffect(() => {
    if (isMobileOpen) {
      hideScroll();
    } else {
      showScroll();
    }
    window.addEventListener("resize", resetNav);
    return () => {
      window.removeEventListener("resize", resetNav);
    };
  }, []);

  useEffect(() => {
    if (isMobileOpen) {
      hideScroll();
    } else {
      showScroll();
    }
  }, [isMobileOpen]);

  // useEffect(() => {
  //   resetNav();
  // }, [userInnerWidth])

  return (
    <header
      className={`section-outer section-header ${
        isMobileOpen ? "section-header--active-nav" : ""
      }`}
    >
      <div className="section-inner section-inner--wide vertical-indent vertical-indent--s">
        <div className="main-navigation">
          <Link to="/" className="logo" aria-label="Logo">
            <span className="visually-hidden">Logo</span>
          </Link>
          <button
            onClick={() => setIsMobileOpen(!isMobileOpen)}
            className="btn btn--no-bg btn-burger"
            aria-label="Toggle navigation"
          >
            <span className="btn-burger__line" />
            <span className="btn-burger__line" />
            <span className="btn-burger__line" />
          </button>
          <HeaderInnerContent />
        </div>
      </div>
    </header>
  );
};

const HeaderInnerContent = () => {
  return (
    <div className={`main-navigation__inner-wrap`}>
      <nav className="nav">
        <ul className="nav-wrapper">
          <li className="nav-wrapper__item">
            <Link
              to="/"
              className="nav-wrapper__item-link nav-wrapper__item-link--active"
            >
              Home
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Blog
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Features
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Pricing
            </Link>
          </li>
          <li className="nav-wrapper__item">
            <Link to="/" className="nav-wrapper__item-link">
              Documentation
            </Link>
          </li>
        </ul>
      </nav>
      <div className="socials">
        <ul className="socials-wrapper">
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgFacebook />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgTelegram />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgTwitter />
            </a>
          </li>
          <li className="socials-wrapper__item">
            <a href="/" className="socials-wrapper__item-link">
              <ImgMedium />
            </a>
          </li>
        </ul>
      </div>
      <a href="/" className="btn btn--primary">
        Get started
      </a>
    </div>
  );
};

export default Header;
